#include <iostream>
#include "Poco/Net/SocketReactor.h"

#include "header/Server.hpp"

int main() {
  spdlog::warn("Starting server...");

  spdlog::info("Name program: {}", compiler_config::NAME_PROGRAM);
  spdlog::info("Port: {}", compiler_config::PORT);

  Server server(compiler_config::PORT);
  server.start();

  spdlog::warn("Server done!");

  std::cin.get();

  spdlog::warn("Server stoping...");
  server.stop();
  spdlog::warn("Server exit");

  return EXIT_SUCCESS;
}
