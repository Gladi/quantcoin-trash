#pragma once

#include <cstdint>

namespace quantcoin_api {
//! Errors while processing your contract
    enum class IContractException : uint_least8_t {
        STRING_IS_EMPTY, //!< String is empty
        TRANSACTION_NOT_MATCH_THE_CONTRACT, //!< The transaction does not match the identifier associated with the contact
        PARSE, //!< An error occurred while parsing
        WRONG_DELIMITER, //!< The transaction has the wrong delimiter. Parsing is not possible
        UNKNOWN //!< Unknown error
    };
};