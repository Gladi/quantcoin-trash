#pragma once

#include <string>
#include <vector>

namespace quantcoin_api {
    using vector_string_t = std::vector<std::string>;

//! Virtual class to store data for your transaction
    struct IContractData {
        //! Parsing the argument vector for exposing your variables
        virtual void parse(const vector_string_t &data) = 0;

        //! Translating your variables into a string
        [[nodiscard]] virtual std::string get_string() const = 0;
    };
};