#pragma once

#include <string>
#include "fmt/format.h"
#include <sstream>
#include <tuple>
#include <vector>

namespace quantcoin_api {
namespace macros {
static constexpr const char SPLIT = '|';
static constexpr const char* const GET_VERSION = "GET_VERSION";
static constexpr const char* const NO_IP_FOR_DICTIONARY = "NO_IP_FOR_DICTIONARY";
};  // namespace macros

namespace detail {
static constexpr const char* const ADD_DICTIONARY_OF_IP = "ADD_DICTIONARY";
static constexpr const char* const SEND_MESSAGE_FOR_ALL_PEERS = "SEND_MESSAGE_FOR_ALL_PEERS";

//! Split
[[nodiscard]] inline static std::vector<std::string> split(
    const std::string& string,
    const char& delimiter) {
  std::vector<std::string> result;
  std::stringstream ss(string);
  std::string item;

  while (getline(ss, item, delimiter))
    result.push_back(item);

  return result;
};

namespace get_dictionary {
static constexpr const char* const GET_DICTIONARY_OF_IP = "GET_DICTIONARY";
static constexpr const char SPLIT_FOR_DICTIONARY_OF_IP_PAIR = '|';
static constexpr const char SPLIT_FOR_DICTIONARY_OF_IP_VECTOR = '?';
static constexpr size_t SIZE_BUFFER_FOR_ONE_IP_PAIR = 100;

using ip_address_t = std::string;
using port_t = std::string;
using info_peer_t = std::tuple<ip_address_t, port_t>;
using vector_info_peer_t = std::vector<detail::get_dictionary::info_peer_t>;

static info_peer_t PARSER_IP(const std::string& message) {
  auto args = detail::split(
      message, detail::get_dictionary::SPLIT_FOR_DICTIONARY_OF_IP_PAIR);
  return std::make_tuple(args[0], args[1]);
}

};

};

namespace builder {
__attribute__((unused)) static std::string ADD_DICTIONARY_OF_IP(
    const std::string& ip,
    const std::string& port) {
  return fmt::format("{}{}{}{}{}", "ADD_DICTIONARY", macros::SPLIT, ip,
                     macros::SPLIT, port);
}

__attribute__((unused)) static std::string GET_DICTIONARY_OF_IP(
    const size_t& value_ip_address_for_dictionary) {
  return fmt::format("{}{}{}", detail::get_dictionary::GET_DICTIONARY_OF_IP,
                     macros::SPLIT, value_ip_address_for_dictionary);
}

__attribute__((unused)) static detail::get_dictionary::vector_info_peer_t PARSER_DICTIONARY_OF_IP(
    const std::string& message) {
  auto vector_ips = detail::split(
      message, detail::get_dictionary::SPLIT_FOR_DICTIONARY_OF_IP_VECTOR);

  detail::get_dictionary::vector_info_peer_t result;
  result.reserve(vector_ips.size());

  for (const auto& vector_ip : vector_ips)
    result.push_back(detail::get_dictionary::PARSER_IP(vector_ip));

  return result;
}

__attribute__((unused)) static inline std::string SEND_MESSAGE_FOR_ALL_PEERS(const std::string& message) {
  return fmt::format("{}|{}", detail::SEND_MESSAGE_FOR_ALL_PEERS, message);
}

};  // namespace builder

}; // namespace quantcoin_api
