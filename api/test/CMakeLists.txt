cmake_minimum_required(VERSION 3.18)
project(test)

set(CMAKE_CXX_STANDARD 20)

set(CONAN_DISABLE_CHECK_COMPILER ON)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(test main.cpp)
target_link_libraries(test ${CONAN_LIBS})
