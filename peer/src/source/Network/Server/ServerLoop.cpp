#include "header/Network/Server/ServerLoop.hpp"
#include "header/Blockchain/Blockchain.hpp"

void ServerCommand::Run(ARGS_SERVER_LOOP) {
  std::scoped_lock<std::mutex> lock(mutex);
  auto args = quantcoin_api::detail::split(message, '|');

  if (args[1] == ServerLoopCommand::SEND_INFO_ABOUT_BLOCKCHAIN)
    SendInfoAboutBlockchain(socket, message);
  else if (args[1] == ServerLoopCommand::SEND_BLOCK_IN_BLOCKCHAIN)
    SendBlockInBlockchain(socket, message);
  else if (args[1] == ServerLoopCommand::SEND_COINS)
    SendCoins(socket, message);
  else if (args[1] == ServerLoopCommand::ADD_NEW_ACCOUNT)
    AddNewAccount(socket, message);
  else
    spdlog::debug("ServerCommand: NOT FOUND COMMAND");
}

void ServerCommand::AddNewAccount(ARGS_SERVER_LOOP) {
  auto args = quantcoin_api::detail::split(message, '|');
  auto block = BlockService::ParserString(args[2]);
  auto value_for_blockchain = args[1];
  spdlog::debug(
      "ServerCommand::AddNewAccount: message = {}, address_wallet = {}, "
      "public_key = {}",
      message, value_for_blockchain, block.get_string());
  // TODO Доделать проверку wallet address и public_key
}

void ServerCommand::GetBlock(ARGS_SERVER_LOOP) {
  auto all_files =
      BlockchainFiles::GetFiles(compiler_config::PATH_TO_BLOCKCHAIN,
                                compiler_config::DEV::LEVELDB_FILE_EXTENSION);

  for (const auto& file : all_files) {
    // TODO
    // socket.sendBytes(file);
  }
}

void ServerCommand::SendInfoAboutBlockchain(ARGS_SERVER_LOOP) {
  spdlog::debug("ServerCommand::SendInfoAboutBlockchain");
  auto args = quantcoin_api::detail::split(message, '|');
  auto blockchain_info = Blockchain::GetInfo();

  const auto answer = fmt::format("{}|{}", blockchain_info.total_blocks,
                                  blockchain_info.root_hash);
  socket.sendBytes(answer.c_str(), answer.size());  // Уже подключен другой пир
}

void ServerCommand::SendBlockInBlockchain(ARGS_SERVER_LOOP) {
  spdlog::debug("ServerCommand::SendBlockInBlockchain");
}

void ServerCommand::SendCoins(ARGS_SERVER_LOOP) {
  spdlog::debug("ServerCommand::SendCoins");
}

void ServerLoop::onReadable(ReadableNotification* pNf) {
  pNf->release();
  char buffer[4000];
  int n = _socket.receiveBytes(buffer, sizeof(buffer));
  if (n > 0) {
    std::string message(buffer, n);
    spdlog::debug("Server new message: {}", message);

    std::thread work([&]() { ServerCommand::Run(_socket, message); });
    work.detach();
  } else {
    //_socket.shutdownSend();
    // delete this;
  }
}

ServerLoop::~ServerLoop() {
  spdlog::debug("Disconnect client");
  _reactor.removeEventHandler(_socket,
                              Observer<ServerLoop, ReadableNotification>(
                                  *this, &ServerLoop::onReadable));
}
