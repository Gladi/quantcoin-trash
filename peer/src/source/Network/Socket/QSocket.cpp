// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Network/Socket/QSocket.hpp"
#include "spdlog/spdlog.h"

using namespace std::chrono_literals;

void QSocket::connect(const std::string& host_address,
                      const uint16_t& port_number) {
    std::scoped_lock<std::mutex> lock(mutex);
    Poco::Net::SocketAddress address(host_address, port_number);
    socket.connect(address);
    socket.setNoDelay(true);

    /*
    listen_thread = new std::thread([&]() {
        for (;;) {
            std::this_thread::sleep_for(1ms);
            this->receive(); // run_command(message); внутри реализации
        }
    });
    listen_thread->detach();
     */
}

void QSocket::send(const std::string& message) {
    std::scoped_lock<std::mutex> lock(mutex);
  spdlog::debug("Socket send message: {}", message.data());
  socket.sendBytes(message.data(), message.size());
}

void QSocket::disconnect() {
    std::scoped_lock<std::mutex> lock(mutex);
  spdlog::debug("Socket shutdown");
  socket.shutdown();
}

std::string QSocket::receive(const size_t& size_buffer) {
    std::scoped_lock<std::mutex> lock(mutex);
  char* buffer = new char[size_buffer];

  int i = socket.receiveBytes(buffer, size_buffer);
  //if (i > 0)
  //  message = std::string(buffer, i);
  std::string message(buffer, i);

  auto args = quantcoin_api::detail::split(message, '|');
  if (args[0] == quantcoin_api::detail::SEND_MESSAGE_FOR_ALL_PEERS)
      run_command(message);

  delete[] buffer; // TODO проверка для всех пиров и отдельное сообшение

  spdlog::debug("Socket receive message: {}", message);
  return message;
}

void QSocket::run_command(const std::string &command) {
    // TODO Сделать в отдельном потоке listen и использовать Network/Server/Server.hpp

    ServerCommand::Run(socket,command);
}

QSocket::~QSocket() {
    //listen_thread->join();
    //delete listen_thread;
}
