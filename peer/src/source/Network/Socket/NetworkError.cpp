// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/Network/Socket/NetworkError.hpp"

const char* NetworkError::what() const noexcept {
  switch (type_error) {
    case Type::connecting:
      return "connecting fail";
    case Type::version_is_old:
      return "your program is old";
    case Type::socket:
      return "socket is broken";
    case Type::get_dictionary_of_ip:
      return "get dictionary of ip";
    case Type::add_dictionary_of_ip:
      return "add dictionary of ip";
    default:
      return "Unknown error";
  }
}
