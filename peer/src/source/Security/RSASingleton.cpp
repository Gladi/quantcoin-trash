#include "header/Security/RSASingleton.hpp"

std::string RSASingleton::_private_key;
std::string RSASingleton::_public_key;
std::mutex RSASingleton::mutex;

std::string RSASingleton::GetPublicKey() noexcept {
    std::scoped_lock<std::mutex> lock(mutex);
    return _public_key;
}

void RSASingleton::SetPublicKey(std::string public_key) noexcept {
    std::scoped_lock<std::mutex> lock(mutex);
    _public_key = public_key;
}

std::string RSASingleton::GetPrivateKey() noexcept {
    std::scoped_lock<std::mutex> lock(mutex);
    return _private_key;
}

void RSASingleton::SetPrivateKey(std::string private_key) noexcept {
    std::scoped_lock<std::mutex> lock(mutex);
    _private_key = private_key;
}
