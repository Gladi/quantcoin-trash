#include "header/Blockchain/detail/Files/BlockchainFiles.hpp"

std::string BlockchainFiles::GetMerkleTree(const std::string& path,
                                           const std::string& file_extension) {
  auto files_for_hashing = GetFiles(path, file_extension);
  MerkleTree tree;

  auto result = std::async(std::launch::async, [&]() -> std::string {
    for (auto& i : files_for_hashing)
      tree.add(Hash::GenerateSHA256(i));

    return tree.get_root();
  });

  try {
    return result.get();
  } catch (...) {
    return "";
  }
}
