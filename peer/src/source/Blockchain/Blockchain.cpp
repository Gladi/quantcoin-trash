// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Blockchain/Blockchain.hpp"

LevelDB Blockchain::db;
std::mutex Blockchain::mutex;
Blockchain::cache_info Blockchain::cache;

void Blockchain::Open(const std::string& path) {
  std::scoped_lock<std::mutex> lock(mutex);
  db.open(path);
}

Account Blockchain::CreateAccount() {
  std::scoped_lock<std::mutex> lock(mutex);
  cache.is_changed = true;
  auto account = AccountService::Create();

  if (db.is_exists(account.wallet_address))
    CreateAccount();  // Retry

  auto [key, info_account] = BlockchainCommand::builder::ADD_ACCOUNT(account);
  auto normal_key =
      fmt::format("{}{}{}", BlockchainCommand::macros::ADD_ACCOUNT, "_", key);
  auto value = BlockService::Create(info_account).get_string();

  db.write(normal_key, value);

  // TODO Отправлять сообщение о создания нового аккаунта
  Network::SendMessageForAllPeers(
      fmt::format("ADD_NEW_ACCOUNT|{}:{}", normal_key, value));

  return account;
}

Account Blockchain::GetAccount(const std::string& wallet_address) {
  std::scoped_lock<std::mutex> lock(mutex);
  std::string key = fmt::format(
      "{}{}{}", BlockchainCommand::macros::ADD_ACCOUNT, "_", wallet_address);
  const auto value = db.read(key);

  const auto public_key = BlockService::ParserString(value).data;
  return Account{.wallet_address = wallet_address,
                 .public_key = public_key,
                 .private_key = ACCOUNT_NOT_VALUE};
}

void Blockchain::Sync() {
  std::scoped_lock<std::mutex> lock(mutex);
  // Сбор всей инфы

  auto info_other_blockchain = GetInfoBlockchainInOtherPeer();

  std::sort(info_other_blockchain.begin(), info_other_blockchain.end(),
            [](InfoBlockchainInOtherPeer const& lhs,
               InfoBlockchainInOtherPeer const& rhs) -> bool {
              return lhs.count_blocks < rhs.count_blocks;
            });

  auto max_element = info_other_blockchain.at(0);
  if (info_other_blockchain.size() >= 2) {
    auto for_сomparison = info_other_blockchain.at(1);

    if (max_element.count_blocks != for_сomparison.count_blocks)
      throw std::runtime_error(
          "max_element.count_blocks != for_сomparison.count_blocks in "
          "Blockchain::Sync()");
  }

  // Постепенная загрузка
  ServerHandler::SetFinishGetBlocks([]() {});
}

void Blockchain::Close() {
  std::scoped_lock<std::mutex> lock(mutex);
  db.close();
}

bool Blockchain::IsCurrent() {
  std::scoped_lock<std::mutex> lock(mutex);
  return false;
}

BlockchainInfo Blockchain::GetInfo() {
  if (cache.is_changed == false)
    return cache.info;

  std::scoped_lock<std::mutex> lock_mutex(mutex);
  LevelDBGuard lock_leveldb(db);
  size_t total_blocks = 0;

  for (const auto& entry : fs::directory_iterator(db.get_path()))
    if (entry.path().extension() ==
        compiler_config::DEV::LEVELDB_FILE_EXTENSION)
      ++total_blocks;

  cache.is_changed = false;
  cache.info = {
      .root_hash = BlockchainFiles::GetMerkleTree(
          db.get_path(), compiler_config::DEV::LEVELDB_FILE_EXTENSION),
      .total_blocks = total_blocks};

  return cache.info;
}

std::vector<InfoBlockchainInOtherPeer>
Blockchain::GetInfoBlockchainInOtherPeer() {
  std::scoped_lock<std::mutex> lock_mutex(mutex);
  std::vector<InfoBlockchainInOtherPeer> result;
  auto [ip_address, port] = DictionaryOfIP::GetMyInfo();
  auto message_for_other_peer =
      fmt::format("{}|{}|{}", ServerLoopCommand::SEND_INFO_ABOUT_BLOCKCHAIN,
                  ip_address, port);

  bool one_send = false;
  for (const auto& [other_ip_address, other_port] : DictionaryOfIP::Get()) {
    QSocket socket;
    socket.connect(other_ip_address, stoi(other_port));
    socket.send(message_for_other_peer);
    auto answer = socket.receive();

    auto args = quantcoin_api::detail::split(answer, '|');
    result.push_back({.ip_address = other_ip_address,
                      .port = stoi(other_port),
                      .count_blocks = stoull(args[0]),
                      .root_hash = args[1]});

    one_send = true;
  }

  if (one_send == false)
    throw std::runtime_error(
        "No peers to receive information for the blockchain");

  return result;
}

bool Blockchain::SendCoins(const std::string& to,
                           const std::string& from,
                           const uint64_t& balance) {
  std::scoped_lock<std::mutex> lock_mutex(mutex);
  cache.is_changed = true;
  const auto balance_to = db.get_balance(to);

  if (balance_to > balance)
    return false;

  auto [message, is_ok] = Crypto::RSA_API::Encrypt(
      fmt::format("SEND_COINS|{}|{}|{}", to, from, balance),
      RSASingleton::GetPrivateKey());

  if (!is_ok)
    return false;

  // Network::SendMessageForAllPeers(message); TODO

  return true;
}
