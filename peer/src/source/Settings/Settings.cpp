// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com
#include "header/Settings/Settings.hpp"

std::mutex SettingsManager::mutex;

void SettingsManager::Save(const Settings& settings) {
  std::scoped_lock<std::mutex> lock(mutex);

  nlohmann::json json;
  json["Account"] = AccountService::GetString(settings.account_info);

  std::fstream file;
  file.open(compiler_config::PATH_TO_SETTINGS,
            std::ios::in | std::ios::out | std::ios::trunc);

  if (file.is_open() == false)
    throw SettingsError(SettingsError::Type::OpenFile);

  file << json.dump();
  file.flush();
  file.close();
}

Settings SettingsManager::Load() {
  std::scoped_lock<std::mutex> lock(mutex);

  if (!std::filesystem::exists(compiler_config::PATH_TO_SETTINGS))
    return {};

  std::fstream file;
  file.open(compiler_config::PATH_TO_SETTINGS, std::ios::in | std::ios::out);

  if (!file.is_open())
    throw SettingsError(SettingsError::Type::OpenFile);

  std::string for_convert_json((std::istreambuf_iterator<char>(file)),
                               std::istreambuf_iterator<char>());
  nlohmann::json json;
  auto data = json.parse(for_convert_json);

  file.close();
  return {.account_info = AccountService::ParserString(data.at("Account"))};
}
