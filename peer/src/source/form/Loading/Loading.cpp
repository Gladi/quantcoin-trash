// This is an open source non-commercial project. Dear PVS-Studio, please check
// it. PVS-Studio Static Code Analyzer for C, C++, C#, and Java:
// https://pvs-studio.com

#include "header/form/Loading/Loading.hpp"

#include "../../../form/ui_Loading.h"

Loading::Loading(QWidget* parent) : QMainWindow(parent), ui(new Ui::Loading) {
  ui->setupUi(this);

  Loading::AllLoadingWork();
}

Loading::~Loading() {
  delete ui;
}

void Loading::done_work(
    service_manager::work_services_output output,
    const service_manager::message_for_error_t& message_for_error,
    QWidget* widget_loading) noexcept {
  switch (output) {
    case service_manager::successful: {
      MainWindow main_window;
      FormService::WindowSwitch(widget_loading, main_window);
      break;
    }

    case service_manager::successful_but_there_are_problems: {
      QMessageBox::warning(widget_loading, compiler_config::NAME_CRYPTOCURRENCY,
                           "PLEASE, CHECK YOUR SERVICE", QMessageBox::Ok);
      break;
    }

    case service_manager::critical_error: {
      QString text_for_qmessagebox;

      // If the stupid programmer didn't provide an error message
      if (qstd::is_in(message_for_error, "", IService::NO_MESSAGE_ERROR))
        text_for_qmessagebox = "Message error not found!";
      else
        text_for_qmessagebox = QString::fromStdString(message_for_error);

      QMessageBox::warning(widget_loading, compiler_config::NAME_CRYPTOCURRENCY,
                           "Critical error: " + text_for_qmessagebox,
                           QMessageBox::Ok);
      std::abort();
    }
  }
}

void Loading::done_one_service(QProgressBar* progress) noexcept {
  progress->setValue(progress->value() + 1);
}

void Loading::AllLoadingWork() noexcept {
  auto services_for_start = LoadingServiceManager::GetServices();
  ui->progress_loading_service->setMaximum(services_for_start.size());  //-V107

  sboost::signal<void()> done_one_service;
  service_manager::signal_for_all_work_done_t done_work;
  Loading::connect_all_signal(done_one_service, done_work,
                              ui->progress_loading_service, this);

  ServiceStartManager service_manager(services_for_start, done_one_service,
                                      done_work);

  service_manager.start();
}
