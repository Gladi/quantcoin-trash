/********************************************************************************
** Form generated from reading UI file 'Closing.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLOSING_H
#define UI_CLOSING_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_Closing
{
public:

    void setupUi(QDialog *Closing)
    {
        if (Closing->objectName().isEmpty())
            Closing->setObjectName(QString::fromUtf8("Closing"));
        Closing->resize(708, 440);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/resource/icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        Closing->setWindowIcon(icon);

        retranslateUi(Closing);

        QMetaObject::connectSlotsByName(Closing);
    } // setupUi

    void retranslateUi(QDialog *Closing)
    {
        Closing->setWindowTitle(QCoreApplication::translate("Closing", "Closing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Closing: public Ui_Closing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLOSING_H
