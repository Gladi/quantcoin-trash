#pragma once

#include <QDialog>
#include <QMessageBox>

#include "ClosingServiceManager.hpp"
#include "config.hpp"
#include "header/Blockchain/Blockchain.hpp"
#include "header/Service/ServiceStopManager.hpp"
#include "header/Service/Services/AllService.hpp"

namespace Ui {
class Closing;
}

class Closing : public QDialog {
  Q_OBJECT

 public:
  explicit Closing(QWidget* parent = nullptr);
  ~Closing();

 private:
  void create_thread() noexcept;

  Ui::Closing* ui;  //-V122
};
