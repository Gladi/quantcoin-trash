//
// Created by gladi on 04.01.2022.
//

#pragma once

#include "header/Service/Services/AllService.hpp"

using namespace service_manager;

//! Get service for closing
class ClosingServiceManager {
 public:
  static vector_iservices_t GetServices() noexcept;

 private:
  static void AddCoreService(vector_iservices_t& services) noexcept;

  ClosingServiceManager() = delete;
  ~ClosingServiceManager() = delete;
};
