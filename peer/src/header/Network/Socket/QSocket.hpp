#pragma once

#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include <mutex>
#include "../../api/Network/TrackerAPI.hpp"
#include <thread>

#include "header/Network/Server/ServerLoop.hpp"
#include "config.hpp"

using Poco::Net::SocketAddress;
using Poco::Net::StreamSocket;

struct QSocket {
  ~QSocket();

  void connect(const std::string& host_address, const uint16_t& port_number);

  void send(const std::string& message);

  void disconnect();

  std::string receive(
      const size_t& size_buffer = compiler_config::SIZE_BUFFER_FOR_SOCKET);

 private:
  StreamSocket socket;
  std::mutex mutex;
  //std::thread* listen_thread;

  void run_command(const std::string& command);
};
