#pragma once

#include "Poco/Error.h"
#include "Poco/Net/SocketStream.h"
#include "spdlog/spdlog.h"

#include "../../api/Network/TrackerAPI.hpp"
#include "NetworkError.hpp"
#include "QSocket.hpp"
#include "header/Network/DictionaryOfIP/DictionaryOfIP.hpp"
#include "header/Network/HTML.hpp"
#include "header/Network/UPnP.hpp"
#include "header/QSTL/tools/convert.hpp"

struct ConnectionPhases {
 public:
  ConnectionPhases(QSocket& socket) noexcept : socket(socket){};

  void connecting_to_tracker(const std::string& host_address, uint16_t port);

  void check_version();

  void add_dictionary_of_ip();

  void get_dictionary_of_ip();

 private:
  QSocket& socket;
};
