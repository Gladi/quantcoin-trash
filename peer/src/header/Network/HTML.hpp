#pragma once

#include <string>
#include "curl/curl.h"

struct HTMLError : std::exception {
  enum class Type {
    REQUEST_IS_NOT_SUCCESSFUL
  };

  explicit HTMLError(Type type_error) noexcept : type_error(type_error) {};

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case Type::REQUEST_IS_NOT_SUCCESSFUL:
        return "request to web is not successful";
      default:
        return "Unknown error";
    }
  };

  Type type_error;
};

namespace HTML {
namespace detail {
static int WriterForCurl(char* data,
                  size_t size,
                  size_t nmemb,
                  std::string* writerData) {
  if (writerData == NULL)
    return 0;
  writerData->append(data, size * nmemb);
  return size * nmemb;
}
};

namespace useful_links {
static constexpr const char* const GET_IP_ADDRESS = "http://ifconfig.me";
};

__attribute__((unused)) static std::string GetContent(const std::string& html_address) {
  CURL *curl = nullptr;
  std::string content;
  curl = curl_easy_init();

  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, html_address.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &content);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, detail::WriterForCurl);
    CURLcode code = curl_easy_perform(curl);

    if (code != CURLE_OK)
      throw HTMLError(HTMLError::Type::REQUEST_IS_NOT_SUCCESSFUL);

    curl_easy_cleanup(curl);
  }

  return content;
};
};
