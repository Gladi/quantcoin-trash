#pragma once

#include <exception>

struct SettingsError : std::exception {
  enum class Type { OpenFile };

  explicit SettingsError(Type type_error) noexcept : type_error(type_error) {}

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case Type::OpenFile:
        return "Settings error: open config file";
      default:
        return "Settings error: unknown error";
    }
  }

  Type type_error;
};