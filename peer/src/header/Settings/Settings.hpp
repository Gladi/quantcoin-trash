#pragma once

#include <filesystem>
#include <fstream>
#include <mutex>
#include "nlohmann/json.hpp"

#include "SettingsError.hpp"
#include "header/Blockchain/Account/Account.hpp"

struct Settings {
  Account account_info;
};

struct SettingsManager {
  static void Save(const Settings& settings);

  static Settings Load();

 private:
  static std::mutex mutex;
};
