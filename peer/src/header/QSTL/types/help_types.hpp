//
// Created by gladi on 27.11.2021.
//

#pragma once

namespace qstd {
    //! Is the execution successful?
    using is_ok_t = bool;

    //! String for constexpr
    //! \warning constexpr std::string only g++-12 but I use Debian 11. Debian 11 use only g++-10
    //! \example
    //! \code
    //! constexpr qstd::constexpr_string = "Touhou fumo";
    //! \endcode
    using constexpr_string = char const*;
    };