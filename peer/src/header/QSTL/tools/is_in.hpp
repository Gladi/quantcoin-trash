//
// Created by gladi on 01.01.2022.
//

#pragma once

namespace qstd {
//! Function that checks given variable first in the list T[]
//! \example
//! \code
//! std::string test = "123";
//! if (qstd::is_in(test, "1", "2", "3", "123")) // "123" is exists!
//!     qstd::printf("done!");
//! \endcode
template <typename First, typename... T>
bool is_in(First&& first, T&&... t)
{
  return ((first == t) || ...);
}
};