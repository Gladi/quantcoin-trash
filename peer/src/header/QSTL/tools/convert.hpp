#pragma once

#include <concepts>
#include <string>

namespace qstd {

template <typename from, typename to>
requires(std::is_convertible_v<from, to>) inline to convert(from obj) {
  return obj;
};

template <typename from, typename to>
requires(
    std::is_same_v<from, std::string>&& std::is_same_v<to, size_t>) inline to
    convert(from obj) {
  to index;
  sscanf(obj.c_str(), "%zu", &index);

  return index;
};

template <typename from, typename to>
requires(
    std::is_same_v<from, uint16_t>&& std::is_same_v<to, std::string>) inline to
    convert(from obj) {
  return std::to_string(obj);
};

};  // namespace qstd
