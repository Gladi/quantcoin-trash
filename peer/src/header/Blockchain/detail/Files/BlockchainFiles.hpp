#pragma once

#include <filesystem>
#include <fstream>
#include <future>
#include <vector>
#include "../../api/Network/TrackerAPI.hpp"

#include "MerkleTree.hpp"
#include "header/Security/Hash.hpp"

namespace fs = std::filesystem;

class BlockchainFiles {
  BlockchainFiles() = delete;
  ~BlockchainFiles() = delete;

 public:
  inline static std::vector<std::string> GetFiles(
      const std::string& path,
      const std::string& file_extension) {
    std::vector<std::string> result;

    for (const auto& entry : fs::directory_iterator(path))
      if (entry.path().extension() == file_extension)
        result.push_back(entry.path());
      else if (entry.path().filename() == "LOG")
        result.push_back(entry.path());
      else if (entry.path().filename() == "CURRENT")
        result.push_back(entry.path());
      else {
        auto args = quantcoin_api::detail::split(entry.path().filename(), '-');

        if (args[0] == "MANIFEST")
          result.push_back(entry.path());
      }

    return result;
  };

  [[nodiscard]] static std::string GetMerkleTree(
      const std::string& path,
      const std::string& file_extension);
};
