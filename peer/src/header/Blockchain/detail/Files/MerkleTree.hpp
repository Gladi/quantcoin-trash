#pragma once

#include <memory>
#include <string>
#include <vector>

#include "header/Security/Hash.hpp"

struct MerkleTreeException : std::exception {
  enum class Type { NotFoundTextForHashing };

  MerkleTreeException(Type type_error) noexcept : type_error(type_error){};

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case Type::NotFoundTextForHashing:
        return "MerkleTree error: Not found text for hashing";
      default:
        return "MerkleTree error: Unknown error";
    }
  };

  Type type_error;
};

struct MerkleTree {
  MerkleTree() = default;
  ~MerkleTree() = default;

  void add(const std::string& hash) noexcept;

  [[nodiscard]] std::string get_root();

 private:
  std::vector<std::string> for_work;

  [[nodiscard]] std::vector<std::string> get_nodes(
      std::vector<std::string>& nodes);

  [[nodiscard]] std::vector<std::string> get_hash(
      std::vector<std::tuple<std::string, std::string>> for_hashing);

  template <typename T>
  [[nodiscard]] inline std::tuple<std::vector<T>, std::vector<T>>
  split_vector_in_half(std::vector<T> vec) {
    std::size_t const half_size = vec.size() / 2;
    std::vector<T> half1(vec.begin(), vec.begin() + half_size);
    std::vector<T> half2(vec.begin() + half_size, vec.end());

    return std::make_tuple(half1, half2);
  }

  [[nodiscard]] inline std::string hashing(const std::string& text1,
                                           const std::string& text2) {
    std::string for_hash = text1 + text2;
    return Hash::GenerateSHA256(for_hash);
  }

  inline void check_on_extra_node(std::unique_ptr<std::string>& extra_node,
                                  std::vector<std::string>& nodes) {
    if (nodes.size() % 2 != 0) {
      if (!extra_node) {
        extra_node = std::make_unique<std::string>(nodes.back());
        nodes.erase(nodes.end());
      } else {
        nodes.push_back(*extra_node);
        extra_node.reset(nullptr);
      }
    }
  };
};
