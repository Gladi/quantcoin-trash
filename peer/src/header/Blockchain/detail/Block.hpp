#pragma once

#include <string>
#include "../../../../../api/Network/TrackerAPI.hpp"
#include "fmt/format.h"

#include "config.hpp"
#include "header/Blockchain/detail/Files/BlockchainFiles.hpp"

struct Block {
  static constexpr const char* const NOT_HASH_ROOT = "NOT";

  Block(std::string hash_root,
        const time_t& timestamp,
        std::string data) noexcept
      : hash_root(std::move(hash_root)),
        data(std::move(data)),
        timestamp(timestamp){};

  std::string hash_root;
  time_t timestamp;
  std::string data;

  [[nodiscard]] std::string get_string() {
    if (hash_root.empty())
      hash_root = NOT_HASH_ROOT;

    return fmt::format("{}#{}#{}", hash_root, timestamp, data);
  };
};

struct BlockService {
  [[nodiscard]] static Block Create(const std::string& data) {
    auto hash_root = BlockchainFiles::GetMerkleTree(
        compiler_config::PATH_TO_BLOCKCHAIN,
        compiler_config::DEV::LEVELDB_FILE_EXTENSION);
    time_t timestamp = time(0);  // Get the system time

    return Block(hash_root, timestamp, data);
  };

  [[nodiscard]] static Block ParserString(const std::string& block_string) {
    auto args = quantcoin_api::detail::split(block_string, '#');

    if (args[0] == Block::NOT_HASH_ROOT)
      args[0] = "";

    return Block(args[0], static_cast<time_t>(atoi(args[1].c_str())), args[2]);
  }
};
