#pragma once

#include <memory>
#include <string>
#include "../../api/Network/TrackerAPI.hpp"
#include "fmt/format.h"
#include "leveldb/db.h"

#include "LevelDBException.hpp"

struct LevelDB {
  LevelDB() = default;

  ~LevelDB() noexcept;

  void open(const std::string& path);

  void write(const std::string& key, std::string value);

  std::string read(const std::string& key);

  bool is_exists(const std::string& key);

  void close();

  [[nodiscard]] std::string get_path() noexcept;

  [[nodiscard]] bool is_open() noexcept;

  uint64_t get_balance(const std::string& wallet_address);

 private:
  leveldb::DB* db;
  std::string path;
  bool _is_open;
};
