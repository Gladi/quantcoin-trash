#pragma once

#include <exception>

struct LevelDBException : std::exception {
  enum class Type { open, write, read };

  LevelDBException(Type type_error) : type_error(type_error){};

  [[nodiscard]] virtual const char* what() const noexcept override {
    switch (type_error) {
      case LevelDBException::Type::open:
        return "LevelDB: error in opening";
      case LevelDBException::Type::write:
        return "LevelDB: error in writing";
      case LevelDBException::Type::read:
        return "LevelDB: error in reading";
      default:
        return "LevelDB: Unknown error";
    }
  };

  Type type_error;
};
