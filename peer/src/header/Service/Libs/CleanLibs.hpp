#pragma once

#include <exception>
#include "curl/curl.h"

//! Stop (clean) library (For very old libraries like libcurl)
//! \warning It's start only in main()!
struct CleanLibs {
  static inline void All() {
    libcurl();
  };

  static inline void libcurl() {
    curl_global_cleanup();
  };
};
