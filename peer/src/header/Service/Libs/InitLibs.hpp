#pragma once

#include <exception>
#include "curl/curl.h"

struct InitLibsError final : std::exception {
  enum class Type {
    LibCurl
  };

  explicit InitLibsError(InitLibsError::Type type_error) noexcept
      : type_error(type_error){};

  [[nodiscard]] const char* what() const noexcept override {
    switch (type_error) {
      case Type::LibCurl:
        return "Error in InitLibsError; LibCurl";
      default:
        return "";
    }
  };

  InitLibsError::Type type_error;
};

//! Start library (For very old libraries like libcurl)
//! \warning It's start only in main()!
struct InitLibs {
  static inline void All() {
      libcurl();
  };

  static inline void libcurl() {
    if (curl_global_init(CURL_GLOBAL_ALL) != CURLE_OK)
      throw InitLibsError(InitLibsError::Type::LibCurl);
  };
};