#pragma once

#include <thread>

#include "header/Network/DictionaryOfIP/DictionaryOfIP.hpp"
#include "header/Network/Server/Server.hpp"
#include "header/Service/Services/IService.hpp"

struct ServerService final : public IService {
  ServerService() = default;
  ServerService(const ServerService&){};
  ServerService& operator=(const ServerService&) = delete;
  ~ServerService() = default;

  IService::type_output_work stop() override;

  IService::type_service get_type() override;

  std::string get_message_error() override;

  std::string get_message_successful() override;

  IService::type_output_work start() override;

  IService::properties get_properties() override;

  bool need_to_stop() override;

 private:
  std::thread* server_thread;
};
