//
// Created by gladi on 04.01.2022.
//

#pragma once

#include "header/Service/Services/Core/Core.hpp"
#include "header/Service/service_manager.hpp"

using namespace service_manager;

class AllService {
 public:
  static vector_iservices_t GetCoreService() noexcept;

 private:
  template <class T>
  static inline service_manager::iservice_ptr create_iservice_ptr(
      T& service) noexcept {
    if constexpr (std::is_base_of_v<IService, T> == false)
      throw std::invalid_argument("IT ISN'T ISERVICE");  //-V509

    return std::make_unique<T>(std::move(service));  //-V833
  }

  template <class T>
  static inline void add_iservice_to_vector(
      service_manager::vector_iservices_t& vector_iservices,
      T& service) noexcept {
    vector_iservices.push_back(create_iservice_ptr(service));
  }

  AllService() = delete;
  ~AllService() = delete;
};
