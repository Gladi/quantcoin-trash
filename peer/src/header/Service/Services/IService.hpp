//
// Created by gladi on 27.11.2021.
//

#pragma once

#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "header/QSTL/types/types.hpp"

//! A service that will do different jobs
//! \warning Add your service in AllService
class IService {
 public:
  enum type_service {
    critical_first,  //!< If the launch fails, the program will crash and
                     //!< service start first
    user,          //!< If the launch fails, the program ignore it. Start after
                   //!< critical_first services
    critical_last  //!< If the launch fails, the program will crash and service
                   //!< start last
  };

  enum properties {
    no_thread,  //!< Prohibiting multithreading
    normal      //!< No properties
  };

  enum type_output_work {
    error,   //!< A fatal error occurred while the service was running
    done,    //!< No error and warning
    warning  //!< A warning occurred while the service was running
  };

  //! For check return value in get_message_error()
  //! \warning DON'T USE IN get_message_error() BECAUSE USE BY DEFAULT
  //! \example
  //! \code
  //! if (service.get_message_error() == ServiceStartManager::NO_MESSAGE_ERROR)
  //!     std::cout << "service haven't get_message_error()" << std::endl;
  //! \endcode
  static constexpr qstd::constexpr_string NO_MESSAGE_ERROR = "ERROR IN SERVICE";

  //! For check return value in get_message_successful()
  //! \warning DON'T USE IN get_message_successful() BECAUSE USE BY DEFAULT
  //! \example
  //! \code
  //! if (service.get_message_successful() ==
  //! ServiceStartManager::NO_MESSAGE_SUCCESSFUL)
  //!     std::cout << "service haven't get_message_successful()" << std::endl;
  //! \endcode
  static constexpr qstd::constexpr_string NO_MESSAGE_SUCCESSFUL =
      "DONE SERVICE";

  //! Start service
  virtual type_output_work start() {
    std::cout << "ISERVICE USE DEFAULT!\n";
    return type_output_work::done;
  };

  //! Stop service
  virtual type_output_work stop() { return type_output_work::done; };

  //! Get type service
  virtual type_service get_type() { return type_service::user; };

  //! Get error message (for Logger)
  virtual std::string get_message_error() {
    return IService::NO_MESSAGE_ERROR;
  };

  //! Get message successful (for Logger)
  virtual std::string get_message_successful() {
    return IService::NO_MESSAGE_SUCCESSFUL;
  };

  //! Get properties
  virtual properties get_properties() { return properties::normal; };

  virtual bool need_to_stop() { return false; }
};

// I will then rewrite the service systems
