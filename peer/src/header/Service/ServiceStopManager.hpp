//
// Created by gladi on 04.01.2022.
//

#pragma once

#include <atomic>
#include <boost/asio/post.hpp>
#include <boost/asio/thread_pool.hpp>
#include <thread>

#include "header/QSTL/tools/mutex_object.hpp"
#include "service_manager.hpp"

using namespace service_manager;
namespace boost_asio = boost::asio;

//! For stop services
class ServiceStopManager {
 public:
  using stop_services_output_t =
      std::tuple<work_services_output, message_for_error_t>;

  ServiceStopManager(vector_iservices_t& services) noexcept
      : services(services){};
  ServiceStopManager() = delete;
  ~ServiceStopManager() = default;

  //! For stop services
  void work() noexcept;

  stop_services_output_t get_result() noexcept;

 private:
  vector_iservices_t& services;
  std::atomic<bool> have_warning = false;
  qstd::mutex_object<have_critical_error_t> have_critical_error =
      create_have_critical_error();
};
