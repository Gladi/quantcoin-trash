//
// Created by gladi on 04.01.2022.
//

#pragma once

#include <boost/signals2.hpp>
#include <memory>
#include <string>
#include <vector>

#include "header/Service/Services/IService.hpp"

namespace service_manager {
using iservice_ptr = std::unique_ptr<IService>;
using vector_iservices_t = std::vector<iservice_ptr>;

enum work_services_output {
  successful,
  successful_but_there_are_problems,
  critical_error
};

using message_for_error_t = std::string;
using signal_for_all_work_done_t =
    boost::signals2::signal<void(work_services_output,
                                 const message_for_error_t&)>;

using have_error_t = bool;

//! Have critical error
//! \warning Creation through create_have_critical_error
using have_critical_error_t = std::tuple<have_error_t, message_for_error_t>;

[[nodiscard]] inline static have_critical_error_t create_have_critical_error(
    const have_error_t& have_error = false,
    const message_for_error_t& message_for_error =
        IService::NO_MESSAGE_ERROR) noexcept {
  return std::make_tuple(have_error, message_for_error);
}
}  // namespace service_manager
