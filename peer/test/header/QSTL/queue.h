//
// Created by gladi on 27.11.2021.
//

#pragma once

#include <gtest/gtest.h>
#include "../../../src/header/QSTL/tools/queue.h"

TEST(QSTL_queue, check_zero_size) {
    qstd::queue<int> test;
    EXPECT_TRUE(test.count() == 0);
}

TEST(QSTL_queue, add_one_element) {
    qstd::queue<int> test;
    test.add(1);
    ASSERT_TRUE(test.count() == 1);
}

TEST(QSTL_queue, get_one_element) {
    qstd::queue<int> test;
    test.add(1);

    qstd::value<int> test_element = test.get_or_nullptr();

    EXPECT_TRUE(*test_element == 1);
}

TEST(QSTL_queue, add_many_element) {
    qstd::queue<int> test;

    for (int i = 0; i < 300; ++i)
        test.add(i);

    EXPECT_TRUE(test.count() == 300);
}

TEST(QSTL_queue, remove_many_element) {
    qstd::queue<int> test;

    for (int i = 1; i < 301; ++i)
        test.add(i);

    for (int i = 1; i < 301; ++i) {
        auto test_get = test.get_or_nullptr();

        if (test_get.exists() == false)
            EXPECT_TRUE(false);
    }

    EXPECT_TRUE(true);
}

TEST(QSTL_queue, check_get_nullptr) {
    qstd::queue<int> test;

    test.add(32);

    test.get_or_nullptr();
    auto test_item = test.get_or_nullptr();

    EXPECT_TRUE(test_item.exists() == false);
}