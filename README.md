# Quantcoin

New framework focused on high performance **(NOT READY)**

## Motivation

> When I use [Monero](https://www.getmonero.org/) I lost 54 rubles when using pools.
> Herefore, I wanted to create a special cryptocurrency that can adapt to your requirements.
> Also, cryptocurrencies are too slow and don't give me the security I want.
> 
> [Gladi](https://gitlab.com/Gladi) or *Author*

### Example: 

If you want maximum speed, then you can turn off security checks, but if you want to turn it back, then you can do it just by clicking on one button

## Screenshot

## TODO

## What does Quantcoin use?

**Language C++20**

- Super nodes **(NOT READY)**
- Transactional memory **(NOT READY)**
- Source-base **(NOT READY)**
- Modularity **(NOT READY)**
- Compiler config
- [Conan](https://conan.io)
- [CMake](https://cmake.org/)
- [Doxygen](http://www.doxygen.nl)

## Compilers for other OC
* Linux = g++
* Windows = MinGW
* MacOS/FreeBSD = Clang

## Licensing

**GNU GPL 3.0**